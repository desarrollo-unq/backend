### Instalar python 3.8
https://tecadmin.net/install-python-3-8-ubuntu/

### Crear venv
```
python3.8 -m venv pedidos-env
```

### Activar venv
```
cd pedidos-env
source bin/activate
```

### Clonar el proyecto

```
git clone https://github.com/pablop94/dapp-grupoi-2020-c1.git
```

### Instalar dependencias
```
cd dapp-grupoi-2020-c1
pip install -r requirements.txt
```

### Correr migraciones
```
python manage.py migrate
```

### Levantar dev server
```
python manage.py runserver
```

## Una vez creado, las proximas veces solo hay que correr
```
source bin/activate
python manage.py runserver
```
