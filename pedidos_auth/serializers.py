from rest_framework import serializers
from rest_framework.authtoken.models import Token
from .models import User

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False)
    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'first_name', 'last_name')

class LoginSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    token = serializers.CharField(source='key')
    class Meta:
        model = Token
        fields = ('token', 'user')


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    new_password = serializers.CharField()

