from rest_framework.routers import DefaultRouter
from .views import *


router = DefaultRouter()
router.register(r'auth', CustomUserViewset, basename='auth')
urlpatterns = router.urls