from django.apps import AppConfig


class PedidosAuthConfig(AppConfig):
    name = 'pedidos_auth'
