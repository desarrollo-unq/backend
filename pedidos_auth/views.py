from datetime import timedelta
from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import viewsets, permissions, status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
from .serializers import *
from .models import User


class CustomUserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()

    def get_serializer_class(self):
        if self.request.method == "put":
            raise Exception("Don't use POST directly, use register instead")
        return UserSerializer

    def update(self, *args, **kwargs):
        response = super().update(*args, **kwargs)

        token = Token.objects.get_or_create(user_id=response.data['id'])[0]
        return Response(LoginSerializer(token).data)

    @action(detail=False, methods=['post'])
    def login(self, request):
        request.data['username'] = request.data['email']
        serializer = AuthTokenSerializer(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token = Token.objects.get_or_create(user=user)[0]
        return Response(LoginSerializer(token).data)

    @action(detail=False, methods=['post'])
    def register(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        user = User(**serializer.validated_data)
        user.set_password(serializer.validated_data['password'])
        user.save()

        token = Token.objects.get_or_create(user=user)[0]
        return Response(LoginSerializer(token).data)

    @action(detail=False, methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def change_password(self, request):
        user = request.user

        serializer = ChangePasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if not user.check_password(serializer.validated_data['old_password']):
            return Response({ "error": "INCORRECT_OLD_PASSWORD" }, status=status.HTTP_403_FORBIDDEN)

        user.set_password(serializer.validated_data['new_password'])    
        user.save()

        Token.objects.filter(user=user).delete()
        token = Token.objects.get_or_create(user=user)[0]

        return Response(LoginSerializer(token).data)