from django.contrib.auth.models import AbstractUser
from django.db import models
from .managers import CustomUserManager



class User(AbstractUser):
    email = models.EmailField(unique=True)
    username = None
    USERNAME_FIELD = 'email'
    objects = CustomUserManager()
    REQUIRED_FIELDS = []