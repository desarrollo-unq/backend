que significa el 10% de las alertas
que significa el umbral
PRODUCTOS RELACIONADOS, VER COMO LO HACEMOS


1) comercio con mas de un rubro? NO
2) horario entrecortado? lo mas razonable  SI
3) comprar en varios comercios, metodos de pago, metodos de envio, ver como resolverlo
4) turnos se asignan por disponibilidad

umbral: tengo un alerta donde no quiero gastar mas de 1000 en bebidas alcoholicas por compra
alerta con % de tolerancia
bloqueante?


un comercio está disponible cuando está a menor o igual distancia de su distancia_maxima
un diahorario está disponible si el día y horario (parametros) están en su rango
un comercio está disponible cuando alguno de sus diahorario está disponible
un diahorario es válido, si su horario_apertura <= horario_cierre
un producto está disponible cuando su stock > 0
el precio de compracomercio_producto es el precio del producto multiplicado por su cantidad
una oferta no puede tener fecha_inicio < fecha_fin
una oferta está activa si la fecha está entre fecha_inicio y fecha_fin
aplicar una oferta_categoria a un producto significa calcular su descuento
aplicar una oferta_producto a un producto significa calcular su descuento

	



modelo usuario
	- nombreUsuario
	- email
	- contraseña

comercio
	- encargado
	- nombre
	- rubro
	- distancia_maxima
	- domicilio
	- geo

geo
	- latitud
	- longitud

medio_de_pago
	- nombre

comerciomedio_de_pago
	- comercio
	- medio_de_pago


diahorario
	- comercio
	- dia
	- horario_apertura
	- horario_cierre

producto
	- nombre
	- categoria
	- marca
	- stock
	- precio
	- imagen
	- comercio

categoria_producto
	- nombre

compra
	- comprador
	- fecha

compracomercio
	- compra
	- comercio
	- monto
	- medio_de_pago
	- tipo_envio


compracomercio_producto
	- compracomercio
	- producto
	- cantidad

tipo_envio
	- nombre

turno
	- compracomercio
	- fecha_horario


envio
	- compracomercio
	- direccion
	- estado

estado_envio
	- nombre
	- codigo

oferta
	- tipo
	- fecha_inicio
	- fecha_fin

tipo_oferta
	- nombre
	- codigo

oferta_categoria
	- categoria
	- porcentaje

oferta_producto
	- porcentaje

relacion_oferta_producto
	- oferta_producto
	- producto
	- descuento


PREGUNTAR:
	- Quieren toda la logica de todos los modelos o sólo la estructura de bd?