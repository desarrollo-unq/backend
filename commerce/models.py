from django.db import models
from django.contrib.auth import get_user_model
from functools import reduce
from django.core.exceptions import ValidationError


User = get_user_model()

class CommerceCategory(models.Model):
	name = models.CharField(max_length=50)

class Geo(models.Model):
	latitude = models.FloatField()
	longitude = models.FloatField()

	def distance(self, other_geo):
		import math
		return math.sqrt((self.latitude - other_geo.latitude)**2 + (self.longitude - other_geo.longitude)**2)

class Commerce(models.Model):
	name = models.CharField(max_length=50)
	address = models.CharField(max_length=50)
	max_distance = models.PositiveSmallIntegerField()
	category = models.ForeignKey(CommerceCategory, on_delete=models.CASCADE)
	supervisor = models.ForeignKey(User, on_delete=models.PROTECT)
	geo = models.ForeignKey(Geo, on_delete=models.PROTECT, null=True)

	def is_available(self, date_time):
		return reduce((lambda ac, dh: ac or dh.is_available(date_time)), self.dayhour_set.all(), False)

	def __str__(self):
		return self.name


class PaymentMethod(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name


class CommercePaymentMethod(models.Model):
	commerce = models.ForeignKey(Commerce, on_delete=models.CASCADE)
	payment_method = models.ForeignKey(PaymentMethod, on_delete=models.PROTECT)

class DayHour(models.Model):
	commerce = models.ForeignKey(Commerce, on_delete=models.CASCADE)
	day = models.PositiveSmallIntegerField()
	opening_hour = models.TimeField() 
	closing_hour = models.TimeField()

class CategoryProduct(models.Model):
	name = models.CharField(max_length=50)


class Product(models.Model):
	name = models.CharField(max_length=50)
	category = models.ForeignKey(CategoryProduct, on_delete=models.PROTECT)
	trademark = models.CharField(max_length=50)
	stock = models.PositiveIntegerField()
	price = models.DecimalField(max_digits=8, decimal_places=2)
	image = models.URLField(max_length=200) 
	commerce = models.ForeignKey(Commerce, on_delete=models.PROTECT)

class Purchase(models.Model):
	purchaser = models.ForeignKey(User, on_delete=models.PROTECT)
	purchase_date = models.DateField(auto_now_add=True)

class ShippingType(models.Model):
	name = models.CharField(max_length=50)

class PurchaseCommerce(models.Model):
	purchase = models.ForeignKey(Purchase, on_delete=models.PROTECT)
	commerce = models.ForeignKey(Commerce, on_delete=models.PROTECT)
	total = models.DecimalField(max_digits=10, decimal_places=2) 
	payment_method = models.ForeignKey(PaymentMethod, on_delete=models.PROTECT)
	shipping_type = models.ForeignKey(ShippingType, on_delete=models.PROTECT)

class PurchaseCommerceProduct(models.Model):
	purchase_commerce = models.ForeignKey(PurchaseCommerce, on_delete=models.CASCADE)
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	amount  = models.PositiveSmallIntegerField()


class Turn(models.Model):
	purchase_commerce = models.ForeignKey(PurchaseCommerce, on_delete=models.CASCADE)
	date_time = models.DateTimeField()

class StateShipping(models.Model):
	name = models.CharField(max_length=50)
	code = models.PositiveSmallIntegerField()

class Shipping(models.Model):
	purchase_commerce = models.ForeignKey(PurchaseCommerce, on_delete=models.CASCADE)
	address = models.CharField(max_length=50)
	state = models.ForeignKey(StateShipping, on_delete=models.PROTECT)

class SaleType(models.Model):
	name = models.CharField(max_length=50)
	code = models.PositiveSmallIntegerField()

class Sale(models.Model):
	sale_type = models.ForeignKey(SaleType, on_delete=models.CASCADE)
	start_date = models.DateField()
	ending_date = models.DateField()

class SaleCategory(models.Model):
	category_product = models.ForeignKey(CategoryProduct, on_delete=models.PROTECT)
	percentage = models.PositiveSmallIntegerField()

class SaleProduct(models.Model):
	percentage = models.PositiveSmallIntegerField()

class RelationSaleProduct(models.Model):
	sale_product = models.ForeignKey(SaleProduct, on_delete=models.CASCADE)
	product = models.ForeignKey(Product, on_delete=models.PROTECT)
	amount = models.PositiveSmallIntegerField()
	discount = models.PositiveSmallIntegerField()

 