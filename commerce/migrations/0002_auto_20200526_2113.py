# Generated by Django 3.0.5 on 2020-05-26 21:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('commerce', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commerce',
            name='geo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='commerce.Geo'),
        ),
    ]
