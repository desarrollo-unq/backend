from huey.contrib.djhuey import task
from django.core.mail import send_mail



@task()
def send_mail_async(subject, message, to):
    send_mail(
        subject,
        '',
        '',
        to,
        html_message=message,
        fail_silently=False,
    )
