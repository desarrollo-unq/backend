from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *


router = DefaultRouter()
router.register(r'commerce', CommerceViewSet, basename='commerce')
router.register(r'commerce-category', CommerceCategoryViewSet, basename='commerce-category')
router.register(r'geo', GeoViewSet, basename='geo')
router.register(r'payment-method', PaymentMethodViewSet, basename='payment-method')
router.register(r'product', ProductViewSet, basename='product')
router.register(r'product-category', CategoryProductViewSet, basename='product-category')
router.register(r'commerce-payment-method', CommercePaymentMethodViewSet, basename='commerce-payment-method')
router.register(r'day-hour', DayHourViewSet, basename='day-hour')
router.register(r'purchase', PurchaseViewSet, basename='purchase')
router.register(r'shipping-type', ShippingTypeViewSet, basename='shipping-type')


urlpatterns = router.urls