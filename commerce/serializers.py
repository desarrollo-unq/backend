from rest_framework import serializers
from .models import *


class PaymentMethodSerializer(serializers.ModelSerializer):
	class Meta:
		model = PaymentMethod
		fields = '__all__'


class CommerceCategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = CommerceCategory
		fields = '__all__'


class CommercePaymentMethodSerializer(serializers.ModelSerializer):
	payment_method = PaymentMethodSerializer(many=False, read_only=True)
	payment_method_id = serializers.PrimaryKeyRelatedField(many=False, write_only=True, source='payment_method', queryset=PaymentMethod.objects.all())

	class Meta:
		model = CommercePaymentMethod
		fields = ('id', 'payment_method', 'payment_method_id', 'commerce')

class CommerceSerializer(serializers.ModelSerializer):
	category = CommerceCategorySerializer(many=False, read_only=True)
	category_id = serializers.PrimaryKeyRelatedField(many=False, queryset=CommerceCategory.objects.all(), write_only=True, source='category')
	payment_methods = CommercePaymentMethodSerializer(many=True, source='commercepaymentmethod_set', read_only=True)

	class Meta:
		model = Commerce
		fields = ('id', 'name', 'address', 'max_distance', 'category', 'category_id', 'supervisor', 'geo', 'payment_methods')


class CommerceCreationSerializer(serializers.ModelSerializer):
	payment_methods = serializers.PrimaryKeyRelatedField(many=True, queryset=PaymentMethod.objects.all())
	class Meta:
		model = Commerce
		fields = ('name', 'address', 'max_distance', 'category', 'payment_methods')


class GeoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Geo
		fields = '__all__'


class CategoryProductSerializer(serializers.ModelSerializer):
	class Meta:
		model = CategoryProduct
		fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):
	category = CategoryProductSerializer(many=False, read_only=True)
	commerce = CommerceSerializer(many=False, read_only=True)
	category_id = serializers.PrimaryKeyRelatedField(many=False, source='category', write_only=True, queryset=CategoryProduct.objects.all())
	commerce_id = serializers.PrimaryKeyRelatedField(many=False, source='commerce', write_only=True, queryset=Commerce.objects.all())
	
	class Meta:
		model = Product
		fields = ('id', 'name', 'category', 'trademark', 'stock', 'price', 'image', 'commerce', 'commerce_id', 'category_id')

class DayHourSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = DayHour
		fields = ('id', 'commerce', 'day', 'opening_hour', 'closing_hour') 


class PurchaseCommerceProductSerializer(serializers.ModelSerializer):
	product = ProductSerializer(many=False, read_only=True)
	product_id = serializers.PrimaryKeyRelatedField(many=False, write_only=True, queryset=Product.objects.all())

	class Meta:
		model = PurchaseCommerceProduct
		fields = ('id', 'product', 'product_id', 'amount')

class PurchaseCommerceSerializer(serializers.ModelSerializer):
	products = PurchaseCommerceProductSerializer(many=True, source='purchasecommerceproduct_set')
	commerce = CommerceSerializer(many=False, read_only=True)
	commerce_id = serializers.PrimaryKeyRelatedField(many=False, write_only=True, source='commerce', queryset=Commerce.objects.all())
	payment_method = PaymentMethodSerializer(many=False, read_only=True)
	payment_method_id = serializers.PrimaryKeyRelatedField(many=False, write_only=True, source='payment_method', queryset=PaymentMethod.objects.all())
	shipping_type = PaymentMethodSerializer(many=False, read_only=True)
	shipping_type_id = serializers.PrimaryKeyRelatedField(many=False, write_only=True, source='shipping_type', queryset=ShippingType.objects.all())

	class Meta:
		model = PurchaseCommerce
		fields = ('id',  'products', 'commerce', 'commerce_id', 'payment_method', 'payment_method_id', 'shipping_type', 'shipping_type_id', 'total')


class PurchaseSerializer(serializers.ModelSerializer):
	purchase_commerces = PurchaseCommerceSerializer(many=True, source='purchasecommerce_set')
	class Meta:
		model = Purchase
		fields = ('id', 'purchase_commerces')


class ShippingTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = ShippingType
		fields = ('id', 'name')