from datetime import datetime
from unittest.mock import patch, MagicMock, PropertyMock
from django.test import SimpleTestCase
from django.core.exceptions import ValidationError
from .models import *
from pedidos_auth.models import User


class GeoTestCase(SimpleTestCase):
    def test_distance_between_two_points(self):
        point1 = Geo(latitude=-7, longitude=-4)
        point2 = Geo(latitude=17, longitude=6.5)

        distance = point1.distance(point2)

        self.assertEquals(distance, 26.196373794859472)

class CommerceAppTestCase:
    def any_category(self):
        return MagicMock(spec=CommerceCategory())
    
    @patch('django.contrib.auth.get_user_model', new_callable=User)
    def any_supervisor(self, user):
        type(user).id = PropertyMock(return_value=3)
        return user

    def any_commerce(self):
        return Commerce(
            name='some name', 
            address='some address', 
            max_distance=5, 
            category=self.any_category(), 
            supervisor=self.any_supervisor(),
            )

class CommerceTestCase(CommerceAppTestCase, SimpleTestCase):
    def test_commerce_is_available_if_has_dayhour_available(self):
        # un comercio está disponible cuando alguno de sus diahorario está disponible

        commerce = self.any_commerce()

        dayhour1 = MagicMock()
        dayhour1.is_available.return_value = False
        dayhour2 = MagicMock()
        dayhour2.is_available.return_value = True

        commerce._prefetched_objects_cache={
            'dayhour_set': [dayhour2, dayhour1]
        }

        self.assertTrue(commerce.is_available(datetime.now()))