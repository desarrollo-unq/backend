from django.contrib import admin
from .models import *


@admin.register(CommerceCategory)
class CommerceCategoryAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('id', 'name',)


@admin.register(PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('id', 'name',)


@admin.register(CommercePaymentMethod)
class CommercePaymentMethodAdmin(admin.ModelAdmin):
    fields = ('commerce', 'payment_method',)
    list_display = ('id', 'commerce', 'payment_method',)


@admin.register(CategoryProduct)
class CategoryProductAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('id', 'name',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    fields = ('name', 'category', 'trademark', 'stock', 'price', 'image', 'commerce',)
    list_display = ('id', 'name', 'category', 'trademark', 'stock', 'price', 'image', 'commerce',)


@admin.register(DayHour)
class DayHourAdmin(admin.ModelAdmin):
    fields = ('day','opening_hour', 'closing_hour', 'commerce',)

@admin.register(ShippingType)
class ShippingTypeAdmin(admin.ModelAdmin):
    fields = ('name',)    