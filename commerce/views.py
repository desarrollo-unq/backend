from rest_framework import viewsets, permissions
from rest_framework.response import Response
from rest_framework.decorators import action
from django.conf import settings
from django.db import transaction
from .serializers import *
from .models import *
from .tasks import send_mail_async


class CommerceViewSet(viewsets.ModelViewSet):
	serializer_class = CommerceSerializer
	queryset = Commerce.objects.all()

	def get_queryset(self):
		user_id = self.request.query_params.get('user')
		if user_id is not None:
			self.queryset = self.queryset.filter(supervisor=user_id)

		return self.queryset


class PurchaseViewSet(viewsets.ModelViewSet):
	serializer_class = PurchaseSerializer
	queryset = Purchase.objects.all().order_by("-id")
	permission_classes = [permissions.IsAuthenticated]

	@action(detail=False, methods=['get'])
	def get_by_user(self, request):
		return Response(self.serializer_class(self.queryset.filter(purchaser=request.user), many=True).data)

	def create(self, request):
		purchase_serializer = self.serializer_class(data=request.data)
		if purchase_serializer.is_valid(raise_exception=True):
			purchase_commerces = purchase_serializer.validated_data.pop('purchasecommerce_set')
			with transaction.atomic():
				purchase = Purchase.objects.create(**purchase_serializer.validated_data, purchaser=request.user)

				purchase_commerce_instances = []
				product_instances = []
				for purchase_commerce in purchase_commerces:
					products = purchase_commerce.pop('purchasecommerceproduct_set')
					instance = PurchaseCommerce.objects.create(**purchase_commerce, purchase=purchase)

					for product in products:
						productid = product.pop('product_id')
						product_instances.append(PurchaseCommerceProduct(**product, product=productid, purchase_commerce=instance))

				PurchaseCommerceProduct.objects.bulk_create(product_instances)

		send_mail_async(f"Se confirmó tu compra número: {purchase.id}", "Se confirmó tu compra!", [request.user.email, *settings.ADMINS])

		return Response(self.serializer_class(instance=purchase).data)

class CommerceCategoryViewSet(viewsets.ModelViewSet):
	serializer_class = CommerceCategorySerializer
	queryset = CommerceCategory.objects.all()


class CommercePaymentMethodViewSet(viewsets.ModelViewSet):
	serializer_class = CommercePaymentMethodSerializer
	queryset = CommercePaymentMethod.objects.all()

	@action(detail=False, methods=['post'])
	def create_many(self, request):
		serializer = self.serializer_class(data=request.data, many=True)

		serializer.is_valid(raise_exception=True)

		serializer.save()
		return Response(serializer.data)


class GeoViewSet(viewsets.ModelViewSet):
	serializer_class = GeoSerializer
	queryset = Geo.objects.all()


class PaymentMethodViewSet(viewsets.ModelViewSet):
	serializer_class = PaymentMethodSerializer
	queryset = PaymentMethod.objects.all()


class CategoryProductViewSet(viewsets.ModelViewSet):
	serializer_class = CategoryProductSerializer
	queryset = CategoryProduct.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
	serializer_class = ProductSerializer
	queryset = Product.objects.all()

	def get_queryset(self):
		commerce_id = self.request.query_params.get('commerce')
		if commerce_id is not None:
			self.queryset = self.queryset.filter(commerce=commerce_id)

		return self.queryset

class DayHourViewSet(viewsets.ModelViewSet):
	serializer_class = DayHourSerializer
	queryset = DayHour.objects.all()

	def get_queryset(self):
		commerce_id = self.request.query_params.get('commerce')
		if commerce_id is not None:
			self.queryset = self.queryset.filter(commerce=commerce_id)

		return self.queryset

	@action(detail=False, methods=['post'])
	def create_many(self, request):
		print(request.data)
		serializer = self.serializer_class(data=request.data, many=True)

		serializer.is_valid(raise_exception=True)

		serializer.save()
		return Response(serializer.data)


class ShippingTypeViewSet(viewsets.ModelViewSet):
	serializer_class = ShippingTypeSerializer
	queryset = ShippingType.objects.all()
